{
  "data": [
    [
      "Tiger Nixon",
      "Retouching",
      "14",
      "5421",
      "2011/04/25",
      "Production"
    ],
    [
      "Garrett Winters",
      "Retouching",
      "30",
      "8422",
      "2011/07/25",
      "Production"
    ],
    [
      "Ashton Cox",
      "Retouching",
      "23",
      "1562",
      "2019/01/12",
      "Production"
    ],
    [
      "Cedric Kelly",
      "Retouching",
      "15",
      "6224",
      "2019/03/29",
      "Production"
    ],
    [
      "Airi Satou",
      "Retouching",
      "10",
      "5407",
      "2019/11/28",
      "Production"
    ],
    [
      "Brielle Williamson",
      "Retouching",
      "25",
      "4804",
      "2019/12/02",
      "Production"
    ],
    [
      "Herrod Chandler",
      "Retouching",
      "23",
      "9608",
      "2019/08/06",
      "Production"
    ],
    [
      "Rhona Davidson",
      "Retouching",
      "10",
      "6200",
      "2019/10/14",
      "Pending"
    ],
    [
      "Colleen Hurst",
      "Retouching",
      "27",
      "2360",
      "2019/09/15",
      "Pending"
    ],
    [
      "Sonya Frost",
      "Retouching",
      "24",
      "1667",
      "2019/12/13",
      "Production"
    ],
    [
      "Jena Gaines",
      "Retouching",
      "38",
      "3814",
      "2019/12/19",
      "Pending"
    ],
    [
      "Quinn Flynn",
      "Retouching",
      "24",
      "9497",
      "2019/03/03",
      "Pending"
    ],
    [
      "Charde Marshall",
      "Retouching",
      "27",
      "6741",
      "2019/10/16",
      "Production"
    ],
    [
      "Haley Kennedy",
      "Retouching",
      "38",
      "3597",
      "2019/12/18",
      "Pending"
    ],
    [
      "Tatyana Fitzpatrick",
      "Retouching",
      "12",
      "1965",
      "2019/03/17",
      "Pending"
    ],
    [
      "Michael Silva",
      "Retouching",
      "16",
      "1581",
      "2019/11/27",
      "Production"
    ],
    [
      "Paul Byrd",
      "Retouching",
      "18",
      "3059",
      "2019/06/09",
      "Pending"
    ],
    [
      "Gloria Little",
      "Retouching",
      "15",
      "1721",
      "2019/04/10",
      "Pending"
    ],
    [
      "Bradley Greer",
      "Retouching",
      "4",
      "2558",
      "2019/10/13",
      "Pending"
    ],
    [
      "Dai Rios",
      "Retouching",
      "46",
      "2290",
      "2019/09/26",
      "For Approval"
    ],
    [
      "Jenette Caldwell",
      "Retouching",
      "65",
      "1937",
      "2011/09/03",
      "For Approval"
    ],
    [
      "Yuri Berry",
      "Retouching",
      "20",
      "6154",
      "2019/06/25",
      "For Approval"
    ],
    [
      "Caesar Vance",
      "Retouching",
      "20",
      "8330",
      "2011/12/12",
      "For Approval"
    ],
    [
      "Doris Wilder",
      "Retouching",
      "15",
      "3023",
      "2019/09/20",
      "For Approval"
    ],
    [
      "Angelica Ramos",
      "Retouching",
      "9",
      "5797",
      "2019/10/09",
      "Production"
    ],
    [
      "Gavin Joyce",
      "Retouching",
      "10",
      "8822",
      "2019/12/22",
      "For Approval"
    ],
    [
      "Jennifer Chang",
      "Retouching",
      "30",
      "9239",
      "2019/11/14",
      "For Approval"
    ],
    [
      "Brenden Wagner",
      "Retouching",
      "4",
      "1314",
      "2011/06/07",
      "Pending"
    ],
    [
      "Fiona Green",
      "Retouching",
      "10",
      "2947",
      "2019/03/11",
      "Production"
    ],
    [
      "Shou Itou",
      "Retouching",
      "15",
      "8899",
      "2011/08/14",
      "Production"
    ],
    [
      "Michelle House",
      "Retouching",
      "5",
      "2769",
      "2019/06/02",
      "Production"
    ],
    [
      "Suki Burks",
      "Retouching",
      "20",
      "6832",
      "2019/10/22",
      "Production"
    ],
    [
      "Prescott Bartlett",
      "Retouching",
      "20",
      "3606",
      "2011/05/07",
      "For Approval"
    ],
    [
      "Gavin Cortez",
      "Retouching",
      "15",
      "2860",
      "2019/10/26",
      "Production"
    ],
    [
      "Martena Mccray",
      "Retouching",
      "12",
      "8240",
      "2011/03/09",
      "Production"
    ],
    [
      "Unity Butler",
      "Retouching",
      "18",
      "5384",
      "2019/12/09",
      "Production"
    ],
    [
      "Howard Hatfield",
      "Retouching",
      "18",
      "7031",
      "2019/12/16",
      "Production"
    ],
    [
      "Hope Fuentes",
      "Retouching",
      "18",
      "6318",
      "2019/02/12",
      "Production"
    ],
    [
      "Vivian Harrell",
      "Retouching",
      "18",
      "9422",
      "2019/02/14",
      "Production"
    ],
    [
      "Timothy Mooney",
      "Retouching",
      "8",
      "7580",
      "2019/12/11",
      "Production"
    ],
    [
      "Jackson Bradshaw",
      "Retouching",
      "25",
      "1042",
      "2019/09/26",
      "Production"
    ],
    [
      "Olivia Liang",
      "Retouching",
      "20",
      "2120",
      "2011/02/03",
      "Production"
    ],
    [
      "Bruno Nash",
      "Retouching",
      "28",
      "6222",
      "2011/05/03",
      "Production"
    ]
  ]
}